// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "YouCube/Simple Torchlight" {
  Properties {
    _Ambient ("Ambient Color", Color) = (0,0,0,1)
    _MainTex ("Base", 2D) = "white" {}
  }
  
  CGINCLUDE

    #include "UnityCG.cginc"

    sampler2D _MainTex;
    float4 _Ambient;
            
    struct v2f {
      half4 pos : SV_POSITION;
      half2 uv : TEXCOORD0;
    };

    v2f vert(appdata_full v) {
      v2f o;
      
      o.pos = UnityObjectToClipPos (v.vertex); 
      o.uv.xy = v.texcoord.xy;
          
      return o; 
    }
    
    fixed4 fragM( v2f i ) : COLOR { 
      fixed4 col = tex2D(_MainTex, i.uv.xy);
      return  col + _Ambient;
    }
  
  ENDCG
  
  SubShader {
    Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
    Cull Off
    Lighting Off
    ZWrite Off
    Fog { Mode Off }
  Pass {
    Blend Zero SrcColor  //multiply
  
    CGPROGRAM
    
    #pragma vertex vert
    #pragma fragment fragM
    #pragma fragmentoption ARB_precision_hint_fastest 
    
    ENDCG
     
    }
  }

  FallBack Off
}
