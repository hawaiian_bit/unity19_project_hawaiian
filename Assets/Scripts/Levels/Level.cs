﻿using UnityEngine;

public abstract class Level : MonoBehaviour
{
  [SerializeField]
  public bool is_dungeon;
  
  public abstract void Init();
  public abstract void Tick();
  public abstract void Clear();
}