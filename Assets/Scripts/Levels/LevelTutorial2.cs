﻿using System.Collections.Generic;
using UnityEngine;

public class LevelTutorial2 : Level
{
  enum LevelLobbyState
  {
    INIT,
    DIALOGUE_1,
    SPAWN_POWERUP,
    COLLECT_POWERUP,
    DIALOGUE_2,
    COLLECT_GREEN,
    GOTO_EXIT
  }
  LevelLobbyState state = LevelLobbyState.INIT; 
  
  [SerializeField]
  public Exit exit_green;
  
  [SerializeField]
  public Transform marker_green_spawn;

  [SerializeField]
  public List<Dialogue.Page> dialogue_1;
  
  [SerializeField]
  public List<Dialogue.Page> dialogue_2;
  
  List<VoxelData> markers_for_spawn;
  public override void Init()
  {
    markers_for_spawn = new List<VoxelData>{WorldData.self.green, WorldData.self.black};
      
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged;
    G.self.player.content.OnContentChanged += OnPlayerContentChanged;
  }
  
  public override void Clear()
  {
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged; 
  }
  
  float offset = 0.5f;
  float gridSize = 1.0f;

  float spawn_markers_stamp;
  float spawn_markers_cooldown = 0.5f;
  float spawn_radius = 30;
  internal Rect spawn_rect = Rect.zero;
  
  const int MAX_CUBES_ON_SCREEN = 20;
  
  bool IsPlayerOnTheLeft()
  {
    return G.self.player.transform.position.x < exit_green.transform.position.x;
  }
  
  public override void Tick()
  {
    if(state == LevelLobbyState.INIT && exit_green.IsActivated())
    {
      state = LevelLobbyState.DIALOGUE_1;
      G.self.dialogue.Show(G.self.player.gameObject, IsPlayerOnTheLeft(), dialogue_1, delegate()
      {
        state = LevelLobbyState.SPAWN_POWERUP;
      });
    }
    else if(state == LevelLobbyState.SPAWN_POWERUP) 
    {
      state = LevelLobbyState.COLLECT_POWERUP;
      Marker red_marker = new GameObject("marker").AddComponent<Marker>();
      red_marker.spawn_params = new SpawnParams(WorldData.self.key, marker_green_spawn.position, Vector3.zero, 1);
      G.self.player.content.RemoveVoxels(1);
    }
    else if (state == LevelLobbyState.COLLECT_GREEN)
    {
      if(G.self.registry.all_cubes.Count >= MAX_CUBES_ON_SCREEN || spawn_markers_stamp + spawn_markers_cooldown > Time.time)
        return;

      Vector3 player_pos = G.self.player.transform.position;
      spawn_markers_stamp = Time.time;
      spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
      spawn_rect.size = new Vector2(spawn_radius * 2, spawn_radius * 2);

      Vector3 start_pos;
      Vector3 direction;
      float random_pos = 2 * spawn_radius * Random.value;

      switch (Random.Range(0, 4))
      {
        case 0:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
          direction = Vector3.right;
          break;
        case 1:
          start_pos = new Vector3(spawn_rect.xMax, 0, spawn_rect.yMax) + Vector3.back * random_pos;
          direction = Vector3.left;
          break;
        case 2:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.right * random_pos;
          direction = Vector3.back;
          break;
        default:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMin) + Vector3.right * random_pos;
          direction = Vector3.forward;
          break;
      }
  
      start_pos -= Vector3.one * offset;
      start_pos /= gridSize;
      start_pos = new Vector3(Mathf.Round(start_pos.x), Mathf.Round(start_pos.y), Mathf.Round(start_pos.z));
      start_pos *= gridSize;
      start_pos += Vector3.one * offset;
  
      Marker random_marker = new GameObject("marker").AddComponent<Marker>();
      random_marker.spawn_params = new SpawnParams(markers_for_spawn[Random.Range(0, markers_for_spawn.Count)], start_pos, direction, 1);
    }
  }

  void OnPlayerContentChanged()
  {
    if(state == LevelLobbyState.COLLECT_POWERUP && G.self.player.content.rows == 2)
    {
      state = LevelLobbyState.DIALOGUE_2;
      G.self.dialogue.Show(G.self.player.gameObject, IsPlayerOnTheLeft(), dialogue_2, delegate()
      {
        state = LevelLobbyState.COLLECT_GREEN;
      });
    }
  }

  public void OnExitGreenConditionStateChanged()
  {
    if(state == LevelLobbyState.COLLECT_GREEN && exit_green.IsSatisfiedToConditions(G.self.player))
    {
      state = LevelLobbyState.GOTO_EXIT;
    }
  }
}