﻿using System.Collections.Generic;
using UnityEngine;

public class LevelTutorial1 : Level
{
  enum LevelLobbyState
  {
    INIT,
    DIALOGUE_1,
    TRY_EXIT,
    DIALOGUE_2,
    SPAWN_RED,
    DIALOGUE_3,
    COLLECT_RED,
    GOTO_EXIT
  }
  LevelLobbyState state = LevelLobbyState.INIT; 
  
  [SerializeField]
  public Exit exit_red;
  
  [SerializeField]
  public Transform marker_red_spawn;

  [SerializeField]
  public List<Dialogue.Page> dialogue_1;
  
  [SerializeField]
  public List<Dialogue.Page> dialogue_2;
  
  [SerializeField]
  public List<Dialogue.Page> dialogue_3;
  
  public override void Init()
  {
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged;
    G.self.player.content.OnContentChanged += OnPlayerContentChanged;
    
    exit_red.OnCubeCollided -= OnExitRedCollided;
    exit_red.OnCubeCollided += OnExitRedCollided;
  }
  
  public override void Clear()
  {
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged; 
    exit_red.OnCubeCollided -= OnExitRedCollided;
  }

  bool IsPlayerOnTheLeft()
  {
    return G.self.player.transform.position.x < exit_red.transform.position.x;
  }

  public override void Tick()
  {
    if(state == LevelLobbyState.INIT && exit_red.IsActivated())
    {
      state = LevelLobbyState.DIALOGUE_1;
      G.self.dialogue.Show(G.self.player.gameObject, IsPlayerOnTheLeft(), dialogue_1, delegate()
      {
        state = LevelLobbyState.TRY_EXIT;
      });
    }
    else if(state == LevelLobbyState.SPAWN_RED) 
    {
      state = LevelLobbyState.DIALOGUE_3;
      Marker red_marker = new GameObject("marker").AddComponent<Marker>();
      red_marker.spawn_params = new SpawnParams(WorldData.self.red, marker_red_spawn.position, Vector3.zero, 1);
      
      G.self.dialogue.Show(G.self.player.gameObject, IsPlayerOnTheLeft(), dialogue_3, delegate()
      {
        state = LevelLobbyState.COLLECT_RED;
      });
    }
  }

  public void OnExitRedCollided(Cube cube)
  {
    if(state == LevelLobbyState.TRY_EXIT && G.self.player == cube)
    {
      state = LevelLobbyState.DIALOGUE_2;
      
      G.self.player.TeleportToPosition(Vector3.right * 3 );
      G.self.dialogue.Show(G.self.player.gameObject, IsPlayerOnTheLeft(), dialogue_2, delegate()
      {
        state = LevelLobbyState.SPAWN_RED;
      });
    }
  }

  void OnPlayerContentChanged()
  {
  }

  public void OnExitRedConditionStateChanged()
  {
    if(state == LevelLobbyState.COLLECT_RED && exit_red.IsSatisfiedToConditions(G.self.player))
    {
      state = LevelLobbyState.GOTO_EXIT;
    }
  }
}