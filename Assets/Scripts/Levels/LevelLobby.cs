﻿using System.Collections.Generic;
using UnityEngine;

public class LevelLobby : Level
{
  enum LevelLobbyState
  {
    INIT,
    DIALOGUE_1,
    SPAWN_RED,
    DIALOGUE_2,
    TAKE_RED,
    GOTO_EXIT
  }
  LevelLobbyState state = LevelLobbyState.INIT; 
  
  [SerializeField]
  public Exit exit_red;
  
  [SerializeField]
  public Transform marker_red_spawn;

  [SerializeField]
  public List<Dialogue.Page> dialogue_1;
  
  [SerializeField]
  public List<Dialogue.Page> dialogue_2;
  
  public override void Init()
  {
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged;
    G.self.player.content.OnContentChanged += OnPlayerContentChanged;
  }
  
  public override void Clear()
  {
    G.self.player.content.OnContentChanged -= OnPlayerContentChanged; 
  }

  public override void Tick()
  {
    if(state == LevelLobbyState.INIT && exit_red.IsActivated())
    {
      state = LevelLobbyState.DIALOGUE_1;
      G.self.dialogue.Show(G.self.player.gameObject, G.self.player.transform.position.x < exit_red.transform.position.x, dialogue_1, delegate()
      {
        state = LevelLobbyState.SPAWN_RED;
        Debug.Log("LevelLobby - OnFinishDialog 1");
      });
    }
    else if (state == LevelLobbyState.DIALOGUE_1)
    {
    }
    else if(state == LevelLobbyState.SPAWN_RED) 
    {
      state = LevelLobbyState.DIALOGUE_2;
      Marker red_marker = new GameObject("marker").AddComponent<Marker>();
      red_marker.spawn_params = new SpawnParams(WorldData.self.red, marker_red_spawn.position, Vector3.zero, 1);
      
      G.self.dialogue.Show(G.self.player.gameObject, G.self.player.transform.position.x < exit_red.transform.position.x, dialogue_2, delegate()
      {
        state = LevelLobbyState.TAKE_RED;
        Debug.Log("LevelLobby - OnFinishDialog 2");
      });
    }
    else if (state == LevelLobbyState.DIALOGUE_2)
    {
    }
    else if (state == LevelLobbyState.TAKE_RED)
    {
    }
    else if (state == LevelLobbyState.GOTO_EXIT)
    {
    }
  }

  void OnPlayerContentChanged()
  {
    Debug.Log("LevelLobby - OnPlayerContentChanged");
    
  }

  public void OnExitRedConditionStateChanged()
  {
    if(state == LevelLobbyState.TAKE_RED && exit_red.IsSatisfiedToConditions(G.self.player))
    {
      state = LevelLobbyState.GOTO_EXIT;
    }

    Debug.Log("LevelLobby - OnConditionStateChanged");
  }
}