﻿using System;
using CnControls;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class G : MonoBehaviour
{
  private static GameState state;
  enum GameState
  {
    LOADING,
    GAME,
    CUT_SCENE,
    VICTORY,
    DEFEAT
  }

  public static G self;

  public GameObject dialogue_go;
  public GameObject arrow_go;
  public Text time;
  public GameObject camera_go;
  public GameObject camera_preview_go;
  public string start_level;
  public WorldData world;
  
  internal Vector3 axis = Vector3.zero;
  
  [NonSerialized]
  public Player player;
  
  [NonSerialized]
  public Level level;

  Transform camera_light_buffer;
  AudioSource ambient_source;
  internal CubesRegistry registry;
  internal Dialogue dialogue;
  
  float left_game_time = 0;
  internal bool is_any_exit_activated = false;

  void Awake()
  {
    self = this;
    registry = new CubesRegistry();
    dialogue = gameObject.AddComponent<Dialogue>();
  }

  void Start()
  {
    state = GameState.LOADING;
    
    arrow_go.SetActive(false);
    dialogue_go.SetActive(false);

    player = FindObjectsOfType<Player>()[0];
    camera_go.GetComponent<PlayerFollower>().Init(player.gameObject);

    camera_light_buffer = camera_go.transform.Find("light_buffer");
    
    ambient_source = gameObject.AddComponent<AudioSource>();
    ambient_source.clip = WorldData.self.ambient;
    ambient_source.loop = true;
    ambient_source.Play();
    
    left_game_time = WorldData.self.game_loop_time;
    
    LoadScene(start_level);
  }

  private string loaded_level_scene = "";
  internal void LoadScene(string scene_name)
  {
    state = GameState.LOADING;
    
    if(level != null)
      level.Clear();
    
    registry.Clear();

    player.TeleportToPosition(Vector3.zero);
    
    if(loaded_level_scene != "")
      SceneManager.UnloadSceneAsync(loaded_level_scene);
    
    SceneManager.sceneLoaded -= OnSceneLoaded;
    SceneManager.sceneLoaded += OnSceneLoaded;
    
    loaded_level_scene = scene_name;
    SceneManager.LoadScene(loaded_level_scene, LoadSceneMode.Additive);
  }
  
  private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
  {
    SceneManager.sceneLoaded -= OnSceneLoaded;
    
    level = FindObjectsOfType<Level>()[0];
    level.Init();
    camera_light_buffer.localPosition = level.is_dungeon ? Vector3.forward * 8 : Vector3.back;
    
    registry.RedrawAllCubes();
    
    state = GameState.GAME;
  }

  private void HardReset()
  {
    SceneManager.LoadScene(0);
  }

  void UpdateTimer()
  {
    float minutes = Mathf.Floor(left_game_time / 60);
    float seconds = Mathf.RoundToInt(left_game_time%60);
    string minutes_str = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();  
    string seconds_str = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();  
    time.text = minutes_str + ":" + seconds_str;
  }

  void Update()
  {
    UpdateTimer();
    
    if (!IsGame())
      return;
      
    ambient_source.volume = player.activity * 0.2f;
    
    axis.x = CnInputManager.GetAxisRaw("Horizontal");
    axis.z = CnInputManager.GetAxisRaw("Vertical");
    
    if(Mathf.Abs(axis.x) > 0.5f && Mathf.Abs(axis.z) < 0.5f)
      axis.Scale(Vector3.right);
    else if(Mathf.Abs(axis.z) > 0.5f && Mathf.Abs(axis.x) < 0.5f)
      axis.Scale(Vector3.forward);
    
    level.Tick();
    registry.Tick();
    
    left_game_time = Mathf.Max(0, left_game_time - Time.deltaTime);
    if (left_game_time == 0)
      GameOver(false);
  }
  
  void OnGUI() {
    if (state == GameState.VICTORY)
    {
      GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), "VICTORY!!!");
      if(GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        HardReset();
      }
      GUI.EndGroup ();
    } 
    else if (state == GameState.DEFEAT)
    {
      GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), "DEFEAT");
      if(GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        HardReset();
      }
      GUI.EndGroup ();
    }
  }

  public static bool IsGame()
  {
    return state == GameState.GAME;
  }
  
  public static void CutSceneState(bool is_dialogue)
  {
    state = is_dialogue ? GameState.CUT_SCENE: GameState.GAME;
  }
  
  public static void GameOver(bool is_victory)
  {
    state = is_victory ? GameState.VICTORY : GameState.DEFEAT;
  }
}
