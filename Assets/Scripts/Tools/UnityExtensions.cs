﻿using UnityEngine;

public static class UnityExtensions
{
  public static void applyRecursivelyOnDescendants(this GameObject gameObject, System.Action<GameObject> action) {
    if (gameObject != null) {
      action(gameObject);
      foreach (Transform transform in gameObject.transform) {
        transform.gameObject.applyRecursivelyOnDescendants(action);
      }
    }
  }
  
  public static GameObject FindChild(this GameObject o, string name)
  {
    Transform t = o.transform.Find(name);
    if(t)
      return t.gameObject;
    return null;
  }
}
