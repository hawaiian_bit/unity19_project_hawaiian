﻿using UnityEngine;

public class Sunflower : MonoBehaviour
{
  Transform sun;
  Transform pivot;

  public float distance;
  Vector3 direction = Vector3.zero;

  void Start()
  {
    sun = Camera.main.transform;
    pivot = transform.parent;
    distance = (transform.position - pivot.position).magnitude;
  }

  void Update()
  {
    direction = (sun.position - pivot.position).normalized;
    transform.position = pivot.position + direction * distance;
    transform.forward = direction;
  }
}