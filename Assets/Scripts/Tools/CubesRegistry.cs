﻿using System.Collections.Generic;

public class CubesRegistry
{
  internal List<Cube> all_cubes = new List<Cube>();

  public void Tick()
  {
    for (int i = 0; i < all_cubes.Count; i++)
    {
      Cube cube = all_cubes[i];
      if(cube != null)
        cube.Tick();
    }
  }

  public void RegisterCube(Cube cube)
  {
    if(cube != null && all_cubes.IndexOf(cube) < 0)
      all_cubes.Add(cube);
  }

  public void UnregisterCube(Cube cube)
  {
    if (cube != null && all_cubes.IndexOf(cube) >= 0)
      all_cubes.Remove(cube);
  }

  public void Clear()
  {
    for (int i = all_cubes.Count - 1; i > 0; i--)
    {
      var cube = all_cubes[i];
      if(cube is Player)
        continue;
      
      all_cubes.RemoveAt(i);
      cube.Kill();
    }
  }

  public void RedrawAllCubes()
  {
    for (int i = all_cubes.Count - 1; i > 0; i--)
    {
      var cube = all_cubes[i];
      if(cube != null)
        cube.content.RedrawContent();
    }
  }
}