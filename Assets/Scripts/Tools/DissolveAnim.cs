using UnityEngine;

public class DissolveAnim : MonoBehaviour
{
  public float scroll_speed = 0.5f;
  Material mat;

  void Start()
  {
    mat = GetComponent<Renderer>().material;
  }

  void OnBecameInvisible() 
  {
    enabled = false;
  }

  void OnBecameVisible() 
  {
    enabled = true;
  }

  void Update() 
  {
    var offset = Time.time * scroll_speed;
    if(mat.HasProperty("_DissolveTex"))
      mat.SetTextureOffset("_DissolveTex", new Vector2(offset,offset));
  }
}