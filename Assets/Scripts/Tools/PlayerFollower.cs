﻿using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
  float speed = 0.1f;
  GameObject target;
  internal Vector3 offset;

  public void Init(GameObject target_)
  {
    target = target_;
    Vector3 target_pos = target.transform.position;
    target_pos.y = 0;
    offset = transform.position - target_pos;
  }

  void LateUpdate()
  {
    if(target == null)
      return;
    
    Vector3 target_pos = target.transform.position;
    target_pos.y = 0;
    Vector3 desired_pos = target_pos + offset;

    if ((desired_pos - transform.position).magnitude > 0.05f)
      transform.position = Vector3.Lerp(transform.position, desired_pos, speed);
  }
}