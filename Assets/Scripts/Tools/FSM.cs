﻿using System;
using UnityEngine;

public class StateMachine<T>
{
  public State<T> currentState { get; private set; }
  public T agent;

  public StateMachine(T _o)
  {
    agent = _o;
    currentState = null;
  }

  public void ChangeState(State<T> _newstate)
  {
    if (currentState != null)
      currentState.ExitState(agent);
    currentState = _newstate;
    currentState.EnterState(agent);
  }

  public void Tick()
  {
    if (currentState != null)
      currentState.UpdateState(agent);
  }
}

public abstract class State<T>
{
  public abstract void EnterState(T _owner);
  public abstract void ExitState(T _owner);
  public abstract void UpdateState(T _owner);
}

public class DummyState : State<Cube>
{
  private static DummyState _instance;

  private DummyState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static DummyState instance
  {
    get
    {
      if (_instance == null)
        new DummyState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
  }
}

public class PlayerInputState : State<Cube>
{
  private static PlayerInputState _instance;

  private PlayerInputState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static PlayerInputState instance
  {
    get
    {
      if (_instance == null)
        new PlayerInputState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    agent.mover.Move(G.self.axis);
  }
}

public class PatrolState : State<Cube>
{
  private static PatrolState _instance;

  private PatrolState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static PatrolState instance
  {
    get
    {
      if (_instance == null)
        new PatrolState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Marker marker = agent.GetComponent<Marker>();
    
    if(marker.speed == Vector3.zero)
      return;

    agent.mover.AgentGotoPos(marker.transform.position + marker.speed.normalized*10);

    if (!agent.mover.IsTargetFound())
      agent.mover.TryFindTarget();
    else
    {
      if (marker.is_stalker())
        agent.ChangeState(PredatorState.instance);
      else if (agent.GetComponent<Marker>().is_walker())
        agent.ChangeState(VictimState.instance);
    }
  }
}

public class PredatorState : State<Cube>
{
  private static PredatorState _instance;

  private PredatorState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static PredatorState instance
  {
    get
    {
      if (_instance == null)
        new PredatorState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Marker marker = agent.GetComponent<Marker>();
    if (marker.is_stalker())
    {
      marker.chase_speed = agent.mover.GetTargetDirection();
      agent.mover.AgentGotoPos(agent.mover.GetTargetPos());

      if (!agent.mover.TargetInRange(agent.mover.attention_radius))
        agent.ChangeState(SearchState.instance);
    }
  }
}

public class VictimState : State<Cube>
{
  private static VictimState _instance;

  private VictimState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static VictimState instance
  {
    get
    {
      if (_instance == null)
        new VictimState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Marker marker = agent.GetComponent<Marker>();
    if (marker.is_walker())
    {
      marker.chase_speed = agent.mover.GetTargetDirection() * -1;
      agent.mover.AgentGotoPos(marker.transform.position + marker.chase_speed.normalized * 10);

      if (!agent.mover.TargetInRange(agent.mover.attention_radius))
        agent.ChangeState(SearchState.instance);
    }
  }
}

public class SearchState : State<Cube>
{
  private static SearchState _instance;

  private SearchState()
  {
    if (_instance != null)
      return;

    _instance = this;
  }

  public static SearchState instance
  {
    get
    {
      if (_instance == null)
        new SearchState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
    agent.mover.search_stamp = Time.time;
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Marker marker = agent.GetComponent<Marker>();
    if (marker.is_stalker() || marker.is_walker())
    {
      agent.mover.AgentGotoPos(marker.transform.position + marker.chase_speed.normalized * 10);

      if (!agent.mover.IsTargetFound())
        agent.mover.TryFindTarget();
      else if (agent.mover.TargetInRange(agent.mover.attention_radius))
      {
        if (marker.is_stalker())
          agent.ChangeState(PredatorState.instance);
        else if (agent.GetComponent<Marker>().is_walker())
          agent.ChangeState(VictimState.instance);
      }

      if (agent.mover.search_stamp + agent.mover.search_duration <= Time.time)
      {
        agent.mover.ForgetTarget();
        agent.ChangeState(PatrolState.instance);
      }
    }
  }
}

public class ExitActivatedState : State<Cube>
{
  private static ExitActivatedState _instance;

  private ExitActivatedState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static ExitActivatedState instance
  {
    get
    {
      if (_instance == null)
        new ExitActivatedState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
//    G.self.player.content.RemoveVoxels((int)Math.Pow(G.self.player.content.rows, 3));
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Exit marker = agent.GetComponent<Exit>();
    marker.transform.Rotate(0.0f, Time.deltaTime * marker.speed_rotation, 0.0f);

    if (marker.has_time_to_exit && marker.GetLeftTimeForExit() == 0)
      marker.DeactivateExit();
    else if (marker.IsSatisfiedToConditions(G.self.player))
      agent.ChangeState(ExitOpenedState.instance);
  }
}

public class ExitDeactivatedState : State<Cube>
{
  private static ExitDeactivatedState _instance;

  private ExitDeactivatedState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static ExitDeactivatedState instance
  {
    get
    {
      if (_instance == null)
        new ExitDeactivatedState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
  }

  public override void ExitState(Cube agent)
  {
  }

  public override void UpdateState(Cube agent)
  {
    Exit marker = agent.GetComponent<Exit>();
    marker.transform.Rotate(0.0f, Time.deltaTime * marker.speed_rotation, 0.0f);

    if (!agent.mover.IsTargetFound())
      agent.mover.TryFindTarget();
    else if (agent.mover.TargetInRange(agent.mover.attention_radius))
      marker.ActivateExit();
  }
}

public class ExitOpenedState : State<Cube>
{
  private static ExitOpenedState _instance;

  private ExitOpenedState()
  {
    if (_instance == null)
      _instance = this;
  }

  public static ExitOpenedState instance
  {
    get
    {
      if (_instance == null)
        new ExitOpenedState();

      return _instance;
    }
  }

  public override void EnterState(Cube agent)
  {
    Exit marker = agent.GetComponent<Exit>();
    marker.ShowPulse(true);

    agent.transform.rotation = Quaternion.identity;
  }

  public override void ExitState(Cube agent)
  {
    Exit marker = agent.GetComponent<Exit>();
    marker.ShowPulse(false);
  }

  public override void UpdateState(Cube agent)
  {
    Exit marker = agent.GetComponent<Exit>();

    if (marker.has_time_to_exit && marker.GetLeftTimeForExit() == 0)
      marker.DeactivateExit();
    else if (!marker.IsSatisfiedToConditions(G.self.player))
      agent.ChangeState(ExitActivatedState.instance);
  }
}