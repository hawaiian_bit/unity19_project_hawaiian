using UnityEngine;

public class LightBuffer : MonoBehaviour
{
  public string camera_name = "camera_light_buffer";
  public GameObject camera_object;
  public RenderTexture render_target;
  public Camera light_buffer_camera;
  public Camera main_camera;
  
  void Start()
  {
    if(camera_object == null)
    {
      camera_object = Camera.main.gameObject.FindChild(camera_name);
      light_buffer_camera = camera_object.GetComponent<Camera>();      
    }
  
    camera_object.SetActive(true);
    main_camera = Camera.main;
  }

  void OnDisable()
  {
    if(camera_object == null)
      return;

    camera_object.SetActive(false);
    camera_object = null;

    if(render_target != null)
    {
      render_target.Release();
      render_target = null;
    }

    if(light_buffer_camera != null)
      light_buffer_camera.targetTexture = null;
  }

  void OnEnable()
  {
    if(camera_object == null)
      return;

    camera_object.SetActive(true);

    if(render_target == null)
    {
      render_target = new RenderTexture(Screen.width / 4, Screen.height / 4, 0, RenderTextureFormat.ARGB32);
      gameObject.GetComponent<MeshRenderer>().material.mainTexture = render_target;
      light_buffer_camera.targetTexture = render_target;
    }
  }

  void Update()
  {
    if(camera_object == null)
      return;
    
    var aspect = (float)Screen.width / (float)Screen.height / main_camera.rect.height;
    light_buffer_camera.aspect = aspect;
    if(main_camera != null)
      transform.localScale = ClipPlaneSize(main_camera); 
  }
  
  Vector3 ClipPlaneSize(Camera cam)
  {
    float spread = cam.farClipPlane - cam.nearClipPlane;
    var size = new Vector3(cam.orthographicSize*2*cam.aspect, cam.orthographicSize*2, spread);
    size.z = 1;
    return size;
  }
}
