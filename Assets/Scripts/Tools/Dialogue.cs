﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
  [Serializable]
  public class Page
  {
    public string name;
    public string message;
    public GameObject character;
  }
  
  List<Page> pages;
  Action on_finish_fn;
  GameObject opponent;
  bool opponent_on_left;
  public void Show(GameObject opponent, bool opponent_on_left, List<Page> pages, Action on_finish_fn)
  {
    this.opponent = opponent;
    this.opponent_on_left = opponent_on_left;
    this.pages = pages;
    this.on_finish_fn = on_finish_fn;
    StartCoroutine(ShowDialog());
  }

  IEnumerator WaitForAnyKey()
  {
    while (!Input.GetKeyDown("space") && !Input.GetMouseButtonDown(0) )
      yield return null;
  }

  int page_idx = 0;
  IEnumerator ShowDialog()
  {
    G.CutSceneState(true);
    GameObject dialogue_go = G.self.dialogue_go;
    dialogue_go.SetActive(true);

    Text title = dialogue_go.transform.Find("title").GetComponent<Text>();
    Text message = dialogue_go.transform.Find("message").GetComponent<Text>();
    GameObject left = dialogue_go.transform.Find("left").gameObject;
    GameObject right = dialogue_go.transform.Find("right").gameObject;

    for (int i = 0; i < pages.Count; i++)
    {
      Page page = pages[i];
      if (page.character == null)
        page.character = opponent;
      
      bool is_opponent = page.character == opponent;
      bool show_left = (opponent_on_left && is_opponent) || (!opponent_on_left && !is_opponent);
      left.SetActive(show_left);
      right.SetActive(!show_left);
    
      title.text = page.name;
      message.text = page.message;
      G.self.camera_preview_go.transform.position = page.character.transform.position + new Vector3(2, 4, -4);
      G.self.camera_preview_go.transform.LookAt(page.character.transform);

      page.character.applyRecursivelyOnDescendants(child => child.layer = LayerMask.NameToLayer("Dialogue"));
    
      yield return new WaitForSeconds(0.5f);
      yield return WaitForAnyKey();
    
      page.character.applyRecursivelyOnDescendants(child => child.layer = LayerMask.NameToLayer("Default"));
    }
  
    dialogue_go.SetActive(false);
    G.CutSceneState(false);
    on_finish_fn?.Invoke();
  }
}