﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Cube))]
public class CubeMover : MonoBehaviour
{
  [NonSerialized]
  public Cube cube;
  [NonSerialized]
  public NavMeshAgent agent;
  
  internal void Init()
  {
    cube = GetComponent<Cube>();

    agent = gameObject.AddComponent<NavMeshAgent>();
    agent.autoBraking = false;
    agent.updatePosition = false;
    agent.updateRotation = false; 
    agent.updateUpAxis = false;
    agent.speed = 1;
  }

  internal void Setup(float width, int speed)
  {
    step_size = width;
    this.speed = speed;
    
    agent.radius = width / 2;
    agent.height = width;
  }

  internal int speed = 10;
  internal int attention_radius = 7;

  private float step_size_;
  public float step_size
  {
    set { step_size_ = value; }
    get { return step_size_; }
  }

  private Vector3 axis_direction = Vector3.zero;
  private Vector3 current_position = Vector3.zero;
  private Quaternion current_rotation = Quaternion.identity;
  private bool toggle = false;
  internal bool is_dead = false;

  private IEnumerator coroutine_animation;

  internal Action on_step_complete;

  internal float search_stamp = 0;
  internal float search_duration = 5;
  Transform target;
  
  public bool IsTargetFound()
  {
    return target != null;
  }

  public void ForgetTarget()
  {
    target = null;
  }

  public Vector3 GetTargetDirection()
  {
    return IsTargetFound() ? target.position - transform.position : Vector3.zero;
  }
  
  public Vector3 GetTargetPos()
  {
    return IsTargetFound() ? target.position : transform.position;
  }

  float GetDistanceToTarget()
  {
    return Vector3.Distance(transform.position, target.position);
  }

  public bool TargetInRange(float distance)
  {
    return GetDistanceToTarget() <= distance;
  }

  public void TryFindTarget()
  {
    Transform potentional_target = G.self.player.transform;
    if (Vector3.Distance(transform.position, potentional_target.position) < attention_radius)
    {
      target = potentional_target;
    }
  }

  internal void Tick()
  {
    
  }

  Vector3 GetNextDirection(Vector3 axis)
  {
    if (axis.sqrMagnitude <= 1)
      return Vector3.one;

    toggle = !toggle;
    return toggle ? Vector3.forward : Vector3.right;
  }

  IEnumerator DoRotation()
  {
    agent.nextPosition = current_position + axis_direction * step_size;
    
    var edge_delta = step_size * 0.5f;
    Vector3 around_pos = current_position + axis_direction * edge_delta;
    around_pos.y = 0;

    Vector3 dir_rotating = Vector3.Cross(Vector3.up, axis_direction);


    float total_time = 1.0f;
    float total_angle = 90.0f;
    float angle = 90.0f;
//    float delta = angle / (float)frames_per_rotation;
    float delta = (total_angle/total_time) * speed * Time.deltaTime;
//    float delta = 50 * frames_per_rotation * Time.deltaTime;

    while (angle > 0)
    {
      if(angle > delta)
        angle -= delta;
      else
      {
        delta = angle;
        angle = 0;
      }
      transform.RotateAround(around_pos, dir_rotating, delta);
      yield return null;
    }
    
    on_step_complete?.Invoke();
    axis_direction = Vector3.zero;
    coroutine_animation = null;
  }

  void StartRotation()
  {
    coroutine_animation = DoRotation();
    StartCoroutine(coroutine_animation);
  }

  IEnumerator ShowStepVfx()
  {
    GameObject vfx = Instantiate(WorldData.self.vfx_step);
    vfx.transform.localScale = Vector3.one * step_size;
    vfx.transform.position = new Vector3(transform.position.x, 0.01f, transform.position.z);
    ParticleSystem ps = vfx.GetComponent<ParticleSystem>();
    ps.startColor = cube.content.GetMixedColor();

    yield return new WaitForSeconds(2);

    Destroy(vfx);
  }

  void Rotate(Vector3 axis)
  {
    if (coroutine_animation != null)
      return;

    current_position = transform.position;
    current_rotation = transform.rotation;
    
    axis.x = axis.x == 0 ? 0 : Mathf.Abs(axis.x) / axis.x;
    axis.y = axis.y == 0 ? 0 : Mathf.Abs(axis.y) / axis.y;
    axis.z = axis.z == 0 ? 0 : Mathf.Abs(axis.z) / axis.z;
    axis_direction = Vector3.Scale(axis, GetNextDirection(axis));
    
    StartRotation();

    StartCoroutine(ShowStepVfx());
  }

  public void AgentGotoPos(Vector3 pos)
  {
    agent.SetDestination(pos);
    
    if(agent.remainingDistance > agent.stoppingDistance)
      Move(agent.desiredVelocity);
  }
  
  public void Move(Vector3 axis)
  {
    if (is_dead || !G.IsGame())
      return;

    if (axis != Vector3.zero)
      Rotate(axis);
  }

  void MarkAsDead()
  {
    cube.ChangeState(DummyState.instance);
    GetComponent<BoxCollider>().enabled = false;
    is_dead = true;
  }

  IEnumerator ShowExplodeVfx()
  {
    while (coroutine_animation != null)
      yield return null;

    List<GameObject> cube_parts = new List<GameObject>();
    int size = 2;
    Vector3 scale = transform.localScale / size;
    Vector3 start_pos = transform.position - Vector3.one * step_size / (size * 2);
    Vector3 marker_pos = transform.position;
    marker_pos.y = 0;
    Vector3 step = Vector3.one * step_size / size;

    for (int i = 0; i < size; i++)
    {
      for (int j = 0; j < size; j++)
      {
        for (int k = 0; k < size; k++)
        {
          Vector3 cube_pos = start_pos + Vector3.Scale(new Vector3(i, j, k), step);
          GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
          cube.name = "cube_part";
          cube.transform.localScale = scale;
          cube.transform.position = cube_pos;
          Renderer cube_renderer = cube.GetComponent<Renderer>();
          var tempMaterial = new Material(cube_renderer.sharedMaterial);
          tempMaterial.color = this.cube.content.GetMixedColor();
          tempMaterial.SetColor("_EmissionColor", Color.clear);
          cube_renderer.sharedMaterial = tempMaterial;

          Rigidbody cube_rb = cube.AddComponent<Rigidbody>();
          cube_rb.AddForce((cube_pos - marker_pos) * 7.5f, ForceMode.Impulse);
          cube_rb.AddTorque(Vector3.up * 720);

          cube_parts.Add(cube);
        }
      }
    }

    cube.content.container.SetActive(false);

    yield return new WaitForSeconds(2);

    foreach (GameObject cube in cube_parts)
    {
      Rigidbody cube_rb = cube.GetComponent<Rigidbody>();
      cube_rb.isKinematic = true;
      cube_rb.useGravity = false;
      Vector3 cube_pos = cube.transform.position;
      cube_pos.y = scale.y / 2;
      cube.transform.position = cube_pos;
    }

    int animation_frames = 60;
    for (int l = 0; l < animation_frames; l++)
    {
      foreach (GameObject cube in cube_parts)
        cube.transform.position = cube.transform.position + Vector3.down * (scale.y / animation_frames);

      yield return null;
    }

    foreach (GameObject cube in cube_parts)
      Destroy(cube);

    cube.Kill();
  }

  internal void Explode()
  {
    if (is_dead)
      return;

    MarkAsDead();
    StartCoroutine(ShowExplodeVfx());
  }

  IEnumerator ShowTakeVfx()
  {
    while (coroutine_animation != null)
      yield return null;

    GameObject vfx = Instantiate(WorldData.self.vfx_vertical_ray);
    vfx.transform.localScale = Vector3.one * step_size;
    vfx.transform.position = new Vector3(transform.position.x, 0.01f, transform.position.z);
    ParticleSystem ps = vfx.GetComponent<ParticleSystem>();
    ps.startColor = cube.content.GetMixedColor();

    cube.content.container.SetActive(false);

    yield return new WaitForSeconds(1);

    Destroy(vfx);
    cube.Kill();
  }

  internal void Take()
  {
    if (is_dead)
      return;

    MarkAsDead();
    StartCoroutine(ShowTakeVfx());
  }

  public void Reset()
  {
    if (coroutine_animation != null)
    {
      StopCoroutine(coroutine_animation);
      coroutine_animation = null;
      
      transform.position = current_position;
      transform.rotation = current_rotation;
    }
  }
}