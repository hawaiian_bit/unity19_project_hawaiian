﻿using System.Collections.Generic;
using UnityEngine;

public class Marker : Cube
{
  private float max_volume = 0.3f;
  private AudioSource step_source;
  internal Vector3 speed;
  internal Vector3 chase_speed;

  internal override void Init()
  {
    base.Init();

    if (spawn_params != null)
    {
      content.rows = spawn_params.rows;
      content.voxels = new List<VoxelData>{spawn_params.data};
      transform.position = spawn_params.pos;
      speed = spawn_params.direction;
    }

    mover.Init();
    content.Init();
    
    step_source = gameObject.AddComponent<AudioSource>();
    step_source.clip = WorldData.self.sfx_step;
    step_source.volume = max_volume;
    step_source.pitch = content.GetVoxelsByFilter(new List<VoxelData>{WorldData.self.black}).Count > 0 ? 0.1f : 50;
    step_source.rolloffMode = AudioRolloffMode.Linear;
    step_source.minDistance = 1;
    step_source.maxDistance = 10;
    step_source.spatialBlend = 1;
    
    mover.on_step_complete = delegate { step_source.Play(); };
    
    ChangeState(PatrolState.instance);
  }
  
  internal override void OnContentChanged()
  {
    base.OnContentChanged();
  }

  void OnBecameInvisible()
  {
    if (!mover.is_dead)
      Destroy(gameObject);
  }
}