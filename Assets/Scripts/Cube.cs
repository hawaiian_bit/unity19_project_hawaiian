﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CubeContent))]
[RequireComponent(typeof(CubeMover))]
public class Cube : MonoBehaviour
{
  [NonSerialized]
  public CubeContent content;
  [NonSerialized]
  public CubeMover mover;
  
  internal SpawnParams spawn_params;
  internal GameObject backpack;
  StateMachine<Cube> state_machine;
  BoxCollider cube_collider;
  
  [Serializable]
  public struct ConditionState
  {
    public enum ConditionValueType
    {
      AMOUNT,
      PERCENT
    }
    
    public enum ConditionOperation
    {
      LT,
      EQ,
      GT
    }
    
    public enum ConditionDirection
    {
      DIRECT,
      INVERSE
    }
    
    [SerializeField]
    internal ConditionValueType type;

    [SerializeField]
    internal ConditionOperation operation;
    
    [SerializeField]
    internal ConditionDirection direction;
    
    [SerializeField]
    internal float value;
    
    [SerializeField]
    internal float radius;
    
    [SerializeField]
    internal List<VoxelData> voxels;
    
    [SerializeField]
    internal UnityEvent OnConditionStateChanged;
    
    [NonSerialized]
    internal List<Cube> satisfied_cubes;

    void StoreResult(Cube cube, bool is_suitable)
    {
      if(satisfied_cubes == null)
        satisfied_cubes = new List<Cube>();

      if (is_suitable && !satisfied_cubes.Contains(cube))
      {
        satisfied_cubes.Add(cube);
        OnConditionStateChanged?.Invoke();
      }
      else if (!is_suitable && satisfied_cubes.Contains(cube))
      {
        satisfied_cubes.Remove(cube);
        OnConditionStateChanged?.Invoke();
      }
    }

    internal bool Check(Cube my_cube, Cube other_cube)
    {
      bool result = false;
      if (radius == 0 || Vector3.Distance(my_cube.transform.position, other_cube.transform.position) <= radius)
      {
        float value = type == ConditionValueType.AMOUNT ? 
          other_cube.content.GetVoxelsByFilter(voxels).Count : 
          other_cube.content.GetPercentOfVoxels(voxels);

        if (operation == ConditionOperation.LT)
          result = value < this.value;
        else if (operation == ConditionOperation.EQ)
          result = value == this.value;
        else if (operation == ConditionOperation.GT)
          result = value > this.value;
      
        result = direction == ConditionDirection.DIRECT ? result : !result;
      }

      StoreResult(other_cube, result);
      return result;
    }
  }

  [SerializeField]
  internal List<ConditionState> conditions = new List<ConditionState>();
  
  public bool IsSatisfiedToConditions(Cube cube)
  {
    for (int i = 0; i < conditions.Count; i++)
    {
      if (conditions[i].satisfied_cubes != null && conditions[i].satisfied_cubes.Contains(cube))
        return true;
    }
    return false;
  }
  
  void CheckForConditions(Cube cube)
  {
    for (int i = 0; i < conditions.Count; i++)
    {
      ConditionState condition = conditions[i];
      bool result = condition.Check(this, cube);
      conditions[i] = condition;
    }
  }

  public bool IsState(State<Cube> state)
  {
    return state_machine.currentState == state;
  }

  internal bool is_player()
  {
    return gameObject.GetComponent<Player>() != null;
  }

  internal bool is_marker()
  {
    return gameObject.GetComponent<Marker>() != null;
  }
  
  internal bool is_stalker()
  {
    return content.GetMixedColor() == Color.magenta;
  }

  internal bool is_walker()
  {
    var all_exits = FindObjectsOfType<Exit>();
    for (int i = 0; i < all_exits.Length; i++)
    {
      Exit exit = all_exits[i];
      if (exit.IsActivated() && exit.content.GetMixedColor() == content.GetMixedColor())
        return true;
    }

    return false;
  }

  internal bool is_exit()
  {
    return gameObject.GetComponent<Exit>() != null;
  }
  
  private bool is_destroyer()
  {
    return content.GetMixedColor() == Color.black || is_stalker();
  }

  private bool is_reverser()
  {
    return content.GetMixedColor() == Color.cyan;
  }
  
  public void ChangeState(State<Cube> state)
  {
    state_machine.ChangeState(state);
  }

  private GameObject shadow;
  private GameObject torchlight;
  private Sunflower sunflower;
  
  internal virtual void Init()
  {
    content = GetComponent<CubeContent>();
    mover = GetComponent<CubeMover>();
    content.OnContentChanged += OnContentChanged;
    
    state_machine = new StateMachine<Cube>(this);
    
    cube_collider = gameObject.AddComponent<BoxCollider>();
    cube_collider.isTrigger = true;

    Rigidbody rb = gameObject.AddComponent<Rigidbody>();
    rb.useGravity = false;
    rb.isKinematic = true;
    rb.angularDrag = 0;
    
    backpack = Instantiate(WorldData.self.backpack);
    torchlight = backpack.FindChild("torchlight");
    shadow = backpack.FindChild("shadow");
    sunflower = torchlight.GetComponent<Sunflower>();
    
    if(G.self)
      G.self.registry.RegisterCube(this);
  }

  internal virtual void OnContentChanged()
  {
    cube_collider.size = Vector3.one * content.GetWidth();
    mover.Setup(content.GetWidth(), content.GetAverageSpeed());

    float cube_width = content.GetWidth();
    shadow.transform.localScale = Vector3.one * cube_width * 2.5f;
    shadow.transform.localPosition = Vector3.down * (cube_width/2) + Vector3.up * 0.01f;
    shadow.SetActive(true);
    
    sunflower.distance = cube_width;
    torchlight.transform.localScale = Vector3.one * cube_width * 7;
    torchlight.SetActive(content.GetVoxelsByFilter(new List<VoxelData> {WorldData.self.torchlight}).Count > 0);
  }
  
  internal virtual void Tick()
  {
    for (int i = 0; i < G.self.registry.all_cubes.Count; i++)
    {
      Cube cube = G.self.registry.all_cubes[i];
      if(cube != this)
        CheckForConditions(cube);
    }

    state_machine.Tick();
    content.Tick();
    mover.Tick();

    backpack.transform.position = transform.position;
  }
  
  public void TeleportToPosition(Vector3 position)
  {
    if (mover == null)
      return;
    
    mover.Reset();
    transform.position = position;
    content.RedrawPosition();
    OnContentChanged();
  }

  void Start()
  {
    Init();
  }
  
  internal Action<Cube> OnCubeCollided;
  void OnTriggerEnter(Collider enemy)
  {
    var other_cube = enemy.gameObject.GetComponent<Cube>();
    if (other_cube != null)
    {
      OnCubeCollided?.Invoke(other_cube);
      WorldData.self.ResolveCollidedPair(this, other_cube);
    }
  }
  
  void OnTriggerExit(Collider enemy)
  {
    var other_cube = enemy.gameObject.GetComponent<Cube>();
    if (other_cube != null)
      WorldData.self.ClearCollidedPair(this, other_cube);
  }

  public virtual void Kill()
  {
    Destroy(backpack);
    if(G.self)
      G.self.registry.UnregisterCube(this);
    
    Destroy(gameObject);
  }
}

public class SpawnParams
{
  public Vector3 direction;
  public Vector3 pos;
  public int rows;
  public VoxelData data;
    
  public SpawnParams(VoxelData data, Vector3 pos, Vector3 direction, int rows)
  {
    this.data = data;
    this.pos = pos;
    this.direction = direction;
    this.rows = rows;
  }
}