﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Cube))]
[ExecuteAlways]
public class CubeContent : MonoBehaviour
{
  internal GameObject container;
  public Voxel[,,] content = new Voxel[1, 1, 1];

  private bool has_changes = false;
  internal Action OnContentChanged; 

  [SerializeField]
  public int rows = 1;
  [SerializeField]
  public float size = 1.0f;
  [SerializeField]
  public float delta = 0.2f;
  
  [SerializeField]
  public List<VoxelData> voxels;
  private List<VoxelData> temp_voxels = new List<VoxelData>();
  
  public float GetWidth()
  {
    return rows * size + Mathf.Max((rows - 1) * delta, 0);
  }
  
  public int GetVoxelsCount()
  {
    return rows * rows * rows;
  }

  public int GetAverageSpeed()
  {
    int accumulated_speed = 0;
    ForeachElements(delegate(int voxel_x, int voxel_y, int voxel_z)
    {
      accumulated_speed += GetOrCreateVoxel(voxel_x, voxel_y, voxel_z).data.GetSpeed();
    });
    
    return Mathf.RoundToInt((accumulated_speed/GetVoxelsCount()) * Math.Max(1, GetWidth()/2) );
  }

  public Color GetMixedColor()
  {
    Color color = Color.clear;
    float intensity = 1.0f / (float)GetVoxelsCount();
    ForeachElements(delegate(int voxel_x, int voxel_y, int voxel_z)
    {
      color += GetOrCreateVoxel(voxel_x, voxel_y, voxel_z).data.GetColor() * intensity;
    });
    color.a = 1;
    return color;
  }
  
  void CreateContainer()
  {
    if (container != null)
      return;
    
    container = new GameObject("container");
    container.hideFlags = HideFlags.DontSave;
    container.transform.parent = transform;
  }

  internal void Init()
  {
    RedrawContent();
  }

  void Start()
  {
    if(!Application.isPlaying)
      RedrawContent();
  }
  
  public void RedrawPosition()
  {
    container.transform.localPosition = new Vector3(-GetWidth() / 2, -GetWidth() / 2, -GetWidth() / 2);
    Vector3 start_pos = transform.position;
    start_pos.y = GetWidth()/2;
    transform.position = start_pos;
  }

  public void RedrawContent()
  {
    temp_voxels.Clear();
    if(voxels != null)
      temp_voxels.AddRange(voxels);
    
    CreateContainer();
    RedrawPosition();
    DestroyVoxels();
    CreateVoxels();
  }

  private void CreateVoxels()
  {
    ForeachElements(delegate(int voxel_x, int voxel_y, int voxel_z)
    {
      Voxel voxel = GetOrCreateVoxel(voxel_x, voxel_y, voxel_z);
      voxel.Draw(IsInternal(voxel_x, voxel_y, voxel_z));
    });
  }
  
  private void DestroyVoxels()
  {
    for (int i = container.transform.childCount; i-- > 0;)
    {
      Voxel voxel = container.transform.GetChild(i).GetComponent<Voxel>();
      StoreVoxelToPool(voxel);
    }
    content = new Voxel[rows, rows, rows];
  }

  public void ForeachElements(Action<int, int, int> cb)
  {
    int voxel_x, voxel_y, voxel_z;
    for (voxel_x = 0; voxel_x < rows; voxel_x++)
    {
      for (voxel_y = 0; voxel_y < rows; voxel_y++)
      {
        for (voxel_z = 0; voxel_z < rows; voxel_z++)
        {
          cb(voxel_x, voxel_y, voxel_z);
        }
      }
    }
  }

  Voxel GetOrCreateVoxel(int voxel_x, int voxel_y, int voxel_z)
  {
    Voxel voxel = content[voxel_x, voxel_y, voxel_z];
    if (voxel == null)
    {
      VoxelData data = null;
      if (temp_voxels.Count > 0)
      {
        data = temp_voxels[0];
        temp_voxels.RemoveAt(0);
      }

      voxel = WriteVoxel(voxel_x, voxel_y, voxel_z, data);
    }

    return voxel;
  }

  bool IsInternal(int voxel_x, int voxel_y, int voxel_z)
  {
    return voxel_x > 0 && voxel_x < rows-1 &&
           voxel_y > 0 && voxel_y < rows-1 && 
           voxel_z > 0 && voxel_z < rows-1;
  }
  
  static private List<Voxel> pool_voxels = new List<Voxel>();

  Voxel GetVoxelFromPool()
  {
    Voxel voxel;
    if (pool_voxels.Count > 0)
    {
      voxel = pool_voxels[0];
      voxel.gameObject.SetActive(true);
      pool_voxels.RemoveAt(0);
      return voxel;
    }

    voxel = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<Voxel>();
    voxel.gameObject.name = "voxel";
    voxel.gameObject.hideFlags = HideFlags.DontSave;
    
    return voxel;
  }
  
  void StoreVoxelToPool(Voxel voxel)
  {
    voxel.Release();
    voxel.gameObject.SetActive(false);
    
    if(Application.isPlaying)
      voxel.transform.parent = G.self.transform;
    
    pool_voxels.Add(voxel);
  }

  Voxel WriteVoxel(int voxel_x, int voxel_y, int voxel_z, VoxelData data)
  {
    Voxel voxel = content[voxel_x, voxel_y, voxel_z];
    if (voxel != null)
      voxel.Init(voxel_x, voxel_y, voxel_z, data);
    else
    {
      voxel = GetVoxelFromPool();
      voxel.Init(voxel_x, voxel_y, voxel_z, data);
      voxel.transform.parent = container.transform;
      voxel.transform.localScale = new Vector3(size, size, size);
      voxel.transform.localPosition =
        new Vector3(voxel_x * size + voxel_x * delta, voxel_y * size + voxel_y * delta,
          voxel_z * size + voxel_z * delta) + Vector3.one * size / 2;

      content[voxel_x, voxel_y, voxel_z] = voxel;
    }
    
    has_changes = true;
    return voxel;
  }

  void SetupVoxel(int voxel_x, int voxel_y, int voxel_z, VoxelData data)
  {
    WriteVoxel(voxel_x, voxel_y, voxel_z, data);
    Voxel voxel = GetOrCreateVoxel(voxel_x, voxel_y, voxel_z);
    voxel.Draw(IsInternal(voxel_x, voxel_y, voxel_z));
  }
  
  Dictionary<List<VoxelData>, List<Voxel>> cached_searching = new Dictionary<List<VoxelData>, List<Voxel>>();
  internal List<Voxel> GetVoxelsByFilter(List<VoxelData> filter)
  {
    List<Voxel> voxels = new List<Voxel>();

    if (cached_searching.ContainsKey(filter))
    {
      cached_searching.TryGetValue(filter, out voxels);
      return voxels;
    }
    
    ForeachElements(delegate(int voxel_x, int voxel_y, int voxel_z)
    {
      Voxel voxel = GetOrCreateVoxel(voxel_x, voxel_y, voxel_z);
      if(filter.Contains(voxel.data) )
        voxels.Add(voxel);
    });
    
    cached_searching.Add(filter, voxels);
    return voxels;
  }
  
  bool GetNextCubePosition(List<VoxelData> filter, out int voxel_x, out int voxel_y, out int voxel_z)
  {
    List<Voxel> voxels = GetVoxelsByFilter(filter);
    if (voxels.Count == 0)
    {
      voxel_x = voxel_y = voxel_z = -1;
      return false;
    }

    Vector3 next_voxel = voxels[0].position;
    voxel_x = (int) next_voxel.x;
    voxel_y = (int) next_voxel.y;
    voxel_z = (int) next_voxel.z;
    return true;
  }
    
  bool GetRandomCubePosition(List<VoxelData> filter, out int voxel_x, out int voxel_y, out int voxel_z)
  {
    List<Voxel> voxels = GetVoxelsByFilter(filter);
    if (voxels.Count == 0)
    {
      voxel_x = voxel_y = voxel_z = -1;
      return false;
    }

    Vector3 random_voxel = voxels[Random.Range(0, voxels.Count)].position;
    voxel_x = (int) random_voxel.x;
    voxel_y = (int) random_voxel.y;
    voxel_z = (int) random_voxel.z;
    return true;
  }
  
  public int GetDamagePoints()
  {
    List<VoxelData> colored_voxels = new List<VoxelData>();
    colored_voxels.Add(WorldData.self.black);
    return GetVoxelsByFilter(colored_voxels).Count + (gameObject.GetComponent<Player>() != null ? 1 : 0);
  }
  
  public int GetHpPoints()
  {
    List<VoxelData> colored_voxels = new List<VoxelData>();
    colored_voxels.AddRange(WorldData.self.GetFoodChain());
    colored_voxels.Add(WorldData.self.any_color);
    return GetVoxelsByFilter(colored_voxels).Count;
  }
  
  public int GetColoredCount()
  {
    return GetVoxelsByFilter(WorldData.self.GetFoodChain()).Count;
  }
  
  public float GetPercentOfVoxels(List<VoxelData> data_voxels)
  {
    return (float)GetVoxelsByFilter(data_voxels).Count/(float)content.Length;
  }

  public List<VoxelData> GetVoxels(List<VoxelData> ignore = null)
  {
    List<VoxelData> data_voxels = new List<VoxelData>();
    ForeachElements(delegate(int voxel_x, int voxel_y, int voxel_z)
    {
      Voxel voxel = GetOrCreateVoxel(voxel_x, voxel_y, voxel_z);
      if(ignore == null || !ignore.Contains(voxel.data))
        data_voxels.Add(voxel.data);
    });
    return data_voxels;
  }

  public void RemoveVoxels(int count)
  {
    for (int i = 0; i < count; i++)
    {
      int voxel_x, voxel_y, voxel_z = 0;
      if (GetNextCubePosition(WorldData.self.GetFoodChain(), out voxel_x, out voxel_y, out voxel_z))
        SetupVoxel(voxel_x, voxel_y, voxel_z, WorldData.self.white);
    }
  }
  
  public void SwapVoxel(VoxelData source, VoxelData data)
  {
    int voxel_x, voxel_y, voxel_z = 0;
    if (GetNextCubePosition(new List<VoxelData>{source}, out voxel_x, out voxel_y, out voxel_z))
      SetupVoxel(voxel_x, voxel_y, voxel_z, data);
  }
  
  public void AddVoxels(List<VoxelData> data_voxels)
  {
    data_voxels.Sort((voxel0, voxel1) => { return voxel0.GetType().CompareTo(voxel1.GetType()); });
    for (int i = 0; i < data_voxels.Count; i++)
    {
      int voxel_x, voxel_y, voxel_z = 0;
      if (GetRandomCubePosition(new List<VoxelData> {WorldData.self.white}, out voxel_x, out voxel_y, out voxel_z))
        SetupVoxel(voxel_x, voxel_y, voxel_z, data_voxels[i]);
      else if (GetRandomCubePosition(new List<VoxelData> {WorldData.self.GetFood(data_voxels[i])}, out voxel_x, out voxel_y, out voxel_z))
        SetupVoxel(voxel_x, voxel_y, voxel_z, data_voxels[i]);
    }
  }

  public void Tick()
  {
    if (has_changes)
    {
      has_changes = false;
      cached_searching.Clear();
      OnContentChanged?.Invoke();
    }
  }
}

public class Voxel : MonoBehaviour
{
  public VoxelData data;
  public Vector3 position = Vector3.zero;
  private Renderer renderer;
  private DissolveAnim dissolve_anim;
  
  public void Init(int x, int y, int z, VoxelData data)
  {
    position.x = x;
    position.y = y;
    position.z = z;
    this.data = data != null ? data : WorldData.self.white;
  }

  public void Draw(bool is_internal)
  {
    if (!renderer)
    {
      BoxCollider box_collider = gameObject.GetComponent<BoxCollider>();
      if(Application.isPlaying)
        Destroy(box_collider);
      else
        DestroyImmediate(box_collider);
      
      renderer = gameObject.GetComponent<Renderer>();
      dissolve_anim = gameObject.AddComponent<DissolveAnim>();
    }
    
    renderer.material = data.GetMaterial();
    renderer.enabled = !is_internal;
      
//    tempMaterial.DisableKeyword("_EMISSION");
//    tempMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.EmissiveIsBlack;
//    tempMaterial.SetColor("_EmissionColor", Color.black);
//    
//    if(Game.self != null && Game.self.is_dungeon)
//      tempMaterial.SetColor("_EmissionColor", new_color);
  }

  public void Release()
  {
    renderer.material = null;
    renderer.enabled = true;
    data = null;
    position = Vector3.zero;
  }
}