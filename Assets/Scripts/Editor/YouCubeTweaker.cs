﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class YouCubeTweaker : EditorWindow
{
  // Add menu named "My Window" to the Window menu
  [MenuItem("YouCube/Tweaker")]
  static void Init()
  {
    // Get existing open window or if none, make a new one:
    YouCubeTweaker window = (YouCubeTweaker)EditorWindow.GetWindow(typeof(YouCubeTweaker));
    window.Show();
  }
  
  private bool restore_focus_on_game = false;
  private void OnEnable()
  {
    SceneManager.sceneLoaded -= OnSceneLoaded;
    SceneManager.sceneLoaded += OnSceneLoaded;
  }

  void RestoreFocusOnGameView()
  {
    EditorApplication.ExecuteMenuItem("Window/General/Game");
  }

  void SelectGameComponent()
  {
    G game_go = FindObjectOfType<G>();
    if(game_go)
      Selection.activeGameObject = game_go.gameObject;
  }

  void OnSceneLoaded(Scene scene, LoadSceneMode mode)
  {
    if(restore_focus_on_game)
    {
      restore_focus_on_game = false;
      SelectGameComponent();
    }
  }

  void OnGUI()
  {
    int scene_count = SceneManager.sceneCountInBuildSettings;
    EditorGUILayout.BeginVertical (GUIStyle.none);
      
    EditorGUILayout.LabelField("--- YouCube Tweaker: useful tools ---");
    
      GUI.enabled = !Application.isPlaying;
      if(GUILayout.Button("PLAY GAME!", GUILayout.Height(60)))
      {
        SelectGameComponent();
        EditorApplication.isPlaying = true;
      }
      GUI.enabled = true;
      
      EditorGUILayout.LabelField("--- GAME SCENES: " + scene_count + " is available for playing ---");
      
      GUI.enabled = Application.isPlaying;
      for (int i = 0; i < scene_count; i++)
      {
        Scene scene = SceneManager.GetSceneByBuildIndex(i);
        string output = i + ": " + scene.name;
        output += scene.isLoaded ? " (Loaded, " : " (Not Loaded, ";
        output += scene.isDirty ? "Dirty)" : "Clean)";
  
        if(GUILayout.Button(output))
        {
          restore_focus_on_game = true;
          SceneManager.LoadScene(i);
        }
      }
      GUI.enabled = true;
      
      EditorGUILayout.LabelField("--- TWEAKS ---");
      
      GUI.enabled = Application.isPlaying;

//      if (GUILayout.Button("Collect color"))
//      {
//        G.self.CollectColor();
//        RestoreFocusOnGameView();
//      }
//
//
//      if (GUILayout.Button("Increase time"))
//      {
//        G.self.IncreaseTime(50);
//        RestoreFocusOnGameView();
//      }
      
      EditorGUILayout.LabelField("--- ADD COLORED ---");
      EditorGUILayout.BeginHorizontal();
      if (GUILayout.Button("red"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.red});
      
      if (GUILayout.Button("blue"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.blue});
      
      if (GUILayout.Button("green"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.green});
      
      EditorGUILayout.EndHorizontal();
      
      EditorGUILayout.LabelField("--- ADD UNIQUE ---");
      EditorGUILayout.BeginHorizontal();
      if (GUILayout.Button("any"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.any_color});
        
      if (GUILayout.Button("key"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.key});
        
      if (GUILayout.Button("time"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.time});
      
      if (GUILayout.Button("reverser"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.reverser});
    
      if (GUILayout.Button("torchlight"))
        G.self.player.content.AddVoxels(new List<VoxelData>{WorldData.self.torchlight});
        
      EditorGUILayout.EndHorizontal();
    
      EditorGUILayout.LabelField("--- REMOVE COLORED ---");
      EditorGUILayout.BeginHorizontal();
      if (GUILayout.Button("1"))
        G.self.player.content.RemoveVoxels(1);
      
      if (GUILayout.Button("2"))
        G.self.player.content.RemoveVoxels(2);
      
      if (GUILayout.Button("3"))
        G.self.player.content.RemoveVoxels(3);
    
      EditorGUILayout.EndHorizontal();
  
      GUI.enabled = true;
    
    EditorGUILayout.EndVertical();
  }
}