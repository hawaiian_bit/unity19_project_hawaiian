﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CubeContent))]
[CanEditMultipleObjects]
public class CubeEditor : Editor
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    
    CubeContent content = target as CubeContent;
    if (content == null)
      return;
    
    EditorGUILayout.BeginVertical (GUIStyle.none);
    
    if(GUILayout.Button("rebuild"))
    {
      content.RedrawContent();
    }
    
    EditorGUILayout.LabelField("--- CUBE STATS ---");
    EditorGUILayout.LabelField("width: " + content.GetWidth());
    
    Exit exit = content.gameObject.GetComponent<Exit>();
    if(exit != null && G.self != null)
    {
      EditorGUILayout.LabelField("--- EXIT STATS ---");
      EditorGUILayout.LabelField("player readyness: " + exit.IsSatisfiedToConditions(G.self.player));
    }
    
    EditorGUILayout.EndVertical();
  }
  
  public void OnSceneGUI()
  {
    CubeContent content = target as CubeContent;
    if (content == null)
      return;
    
    float handle_size = HandleUtility.GetHandleSize(content.transform.position) * 1f;
    float handle_snap = 1.0f;
    Vector3 handle_pos = content.transform.position;
    
    GUIStyle style = new GUIStyle();
    style.richText = true;
    Handles.Label(handle_pos + (Vector3.up + Vector3.right) * content.GetWidth()/2,
      "You can change <color=yellow>rows</color>, <color=cyan>size</color> and <color=magenta>delta</color>.",
      style
    );
    
    Handles.color = Color.yellow;
    EditorGUI.BeginChangeCheck();
    float rows = Handles.ScaleSlider(content.rows, handle_pos, content.transform.up * 1.5f, content.transform.rotation, handle_size, handle_snap);
    if (EditorGUI.EndChangeCheck())
    {
      Undo.RecordObject(target, "Change Rows Value");
      content.rows = Mathf.RoundToInt(rows);
      content.RedrawContent();
    }
    
    Handles.color = Color.cyan;
    EditorGUI.BeginChangeCheck();
    float size = Handles.ScaleSlider(content.size, handle_pos, content.transform.forward * 1.5f, content.transform.rotation, handle_size, handle_snap);
    if (EditorGUI.EndChangeCheck())
    {
      Undo.RecordObject(target, "Change Size Value");
      content.size = size;
      content.RedrawContent();
    }
    
    Handles.color = Color.magenta;
    EditorGUI.BeginChangeCheck();
    float delta = Handles.ScaleSlider(content.delta, handle_pos, content.transform.right * 1.5f, content.transform.rotation, handle_size, handle_snap);
    if (EditorGUI.EndChangeCheck())
    {
      Undo.RecordObject(target, "Change Delta Value");
      content.delta = delta;
      content.RedrawContent();
    }
  }
}