﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(G))]
public class GameEditor : Editor
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    
    EditorGUILayout.BeginVertical (GUIStyle.none);
    
    EditorGUILayout.LabelField("--- GAME STATS ---");

    EditorGUILayout.BeginHorizontal();
    EditorGUILayout.LabelField("Player color state: ");
    GUI.enabled = false;
//    EditorGUILayout.ColorField(G.self != null ? G.self.player.content.GetMixedColor() : Color.white);
    EditorGUILayout.EndHorizontal();
    GUI.enabled = true;
    
    EditorGUILayout.BeginHorizontal();
    EditorGUILayout.LabelField("Exits count: " + FindObjectsOfType<Exit>().Length);
    EditorGUILayout.EndHorizontal();
    
//    EditorGUILayout.BeginHorizontal();
//    EditorGUILayout.LabelField("Marker colors: ", GUILayout.Width(100));
//    GUI.enabled = false;
//
//    if (Application.isPlaying)
//    {
//      for (int i = 0; i < Game.self.GetAvailableColors().Count; i++)
//        EditorGUILayout.ColorField(Game.self.GetAvailableColors()[i]); 
//    }    
//    
//    EditorGUILayout.EndHorizontal();
//    GUI.enabled = true;
    
    
    EditorGUILayout.EndVertical();
  }
}