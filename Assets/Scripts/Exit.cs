﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Exit : Cube
{
  GameObject pulse_go;
  ParticleSystem pulse_ps;
  GameObject arrow_go;
  Image time_go;
  internal Animator time_animator;
  internal float speed_rotation = 50;

  const float max_exit_time = 20;
  float exit_time = 0;

  [SerializeField]
  internal string exit_to_level;
  
  [SerializeField]
  internal bool has_time_to_exit;
  
  
  internal float GetLeftTimeForExit()
  {
    return exit_time;
  }

  internal override void Init()
  {
    base.Init();
    mover.Init();
    content.Init();
    
    arrow_go = Instantiate(G.self.arrow_go, G.self.arrow_go.transform.parent);

    GameObject time_child = arrow_go.transform.Find("time").gameObject;
    time_go = time_child.GetComponent<Image>();
    time_go.fillAmount = 1;

    time_animator = time_child.GetComponent<Animator>();
    time_animator.SetBool("Alert", false);
    time_animator.SetTrigger("Cancel");

    pulse_go = Instantiate(WorldData.self.vfx_exit_pulse, WorldData.self.vfx_exit_pulse.transform.parent);
    pulse_ps = pulse_go.GetComponent<ParticleSystem>();
    pulse_ps.startColor = content.GetMixedColor();

    Vector3 cube_pos = transform.position;  
    cube_pos.y = 0.1f;
    pulse_go.transform.position = cube_pos;
    
    ChangeState(ExitDeactivatedState.instance);
  }

  internal override void Tick()
  {
    bool player_is_satisfied = IsSatisfiedToConditions(G.self.player);
    arrow_go.SetActive(IsActivated() && has_time_to_exit);
    pulse_go.SetActive(IsActivated() && player_is_satisfied);

    float delta = 45;
    Vector3 player_screen_pos = Camera.main.WorldToScreenPoint(G.self.player.transform.position);
    Vector3 exit_screen_pos = Camera.main.WorldToScreenPoint(transform.position);
    Vector3 dir_to_exit = exit_screen_pos - player_screen_pos;
    player_screen_pos.z = 0;
    exit_screen_pos.z = 0;
    Vector3 exit_arrow_screen_pos = exit_screen_pos - dir_to_exit.normalized * 5;

    arrow_go.transform.position = new Vector3(Mathf.Clamp(exit_arrow_screen_pos.x, delta, Screen.width - delta),
      Mathf.Clamp(exit_arrow_screen_pos.y, delta, Screen.height - delta), 0);
    arrow_go.transform.Find("open").right = (exit_screen_pos - arrow_go.transform.position).normalized;
    arrow_go.transform.Find("open").gameObject.SetActive(player_is_satisfied);
    arrow_go.transform.Find("locked").gameObject.SetActive(!player_is_satisfied);
    arrow_go.transform.Find("color").GetComponent<RawImage>().color = content.GetMixedColor();

    if (!IsState(ExitDeactivatedState.instance) && has_time_to_exit)
    {
      exit_time = Mathf.Max(0, exit_time - Time.deltaTime);
      time_go.fillAmount = exit_time / max_exit_time;

      if (GetLeftTimeForExit() > 0)
        time_animator.SetBool("Alert", GetLeftTimeForExit() < 5);
      else
        ChangeState(ExitDeactivatedState.instance);
    }
    else
    {
      time_animator.SetBool("Alert", false);
      time_go.fillAmount = 1;
    }

    base.Tick();
  }
  
  public override void Kill()
  {
    Destroy(arrow_go);
    Destroy(pulse_go);
    
    base.Kill();
  }
  
  void OnTriggerEnter(Collider other)
  {
    var other_cube = other.gameObject.GetComponent<Cube>();
    if (other_cube == null)
      return;
    
    OnCubeCollided?.Invoke(other_cube);
    
    if (!IsActivated() || !IsSatisfiedToConditions(other_cube))
      return;

    if (other_cube.is_player())
      G.self.LoadScene(exit_to_level);
    else
      other_cube.mover.Explode();
  }

  internal void ShowPulse(bool active)
  {
    pulse_ps.emissionRate = active ? 1 : 0;
  }

  internal void ActivateExit()
  {
    G.self.is_any_exit_activated = true;
    exit_time = max_exit_time;
    ChangeState(ExitActivatedState.instance);
  }

  internal void DeactivateExit()
  {
    exit_time = 0;
    ChangeState(ExitDeactivatedState.instance);
  }

  internal void IncreaseTime(int seconds)
  {
    exit_time = Mathf.Min(max_exit_time, exit_time + seconds);
    time_animator.SetTrigger("Add");
  }

  public bool IsActivated()
  {
    return !IsState(ExitDeactivatedState.instance);
  }
}