﻿using System.Collections.Generic;
using UnityEngine;

public class Player : Cube
{
  AudioSource step_source;

  internal float activity;
  float activity_step = 0.01f;

  internal override void Init()
  {
    base.Init();
    mover.Init();
    content.Init();
    
    mover.agent.enabled = false;
    mover.on_step_complete = delegate { step_source.Play(); };
    
    step_source = gameObject.AddComponent<AudioSource>();
    step_source.clip = WorldData.self.sfx_step;
    step_source.volume = 0.3f;

    backpack.GetComponent<AudioListener>().enabled = true;

    ChangeState(PlayerInputState.instance);
  }
  
  internal override void OnContentChanged()
  {
    base.OnContentChanged();
    if (content.GetVoxelsByFilter(new List<VoxelData> {WorldData.self.key}).Count > 0)
    {
      content.SwapVoxel(WorldData.self.key, WorldData.self.white);
      content.rows += 1;
      content.RedrawContent();
    }
  }
  
  internal override void Tick()
  {
    base.Tick();
    activity = Mathf.Clamp01(activity + (G.self.axis.magnitude > 0.1f ? activity_step : activity_step * -1));
  }
}