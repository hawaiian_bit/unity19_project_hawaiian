﻿using UnityEngine;

[CreateAssetMenu(fileName = "New VoxelData", menuName = "Voxel Data", order = 51)]
public class VoxelData : ScriptableObject
{
    public enum VoxelType
    {
        CLEAR,
        BASIC,
        DESTROYER,
        UNIQUE
    }
    [SerializeField]
    private VoxelType type;
    [SerializeField]
    private Color color;
    [SerializeField]
    private Material material;
    [SerializeField]
    private int speed = 10;
    [SerializeField]
    public AudioClip sfx_step;

    public VoxelType GetType() => type;
    public Color GetColor() => color;
    public Material GetMaterial() => material;
    public int GetSpeed() => speed;

    public override string ToString()
    {
        return "voxel: " + color.ToString();
    }
}
