﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New WorldData", menuName = "World Data", order = 51)]
public class WorldData : SingletonSO<WorldData>
{
  [SerializeField] 
  public float round_time = 60;
  
  [SerializeField] 
  public VoxelData white;
  [SerializeField] 
  public VoxelData black;
  [SerializeField] 
  public VoxelData any_color;
  [SerializeField] 
  public VoxelData key;
  [SerializeField] 
  public VoxelData time;
  [SerializeField] 
  public VoxelData reverser;
  [SerializeField] 
  public VoxelData red;
  [SerializeField] 
  public VoxelData green;
  [SerializeField] 
  public VoxelData blue;
  [SerializeField] 
  public VoxelData torchlight;
  
  [SerializeField] 
  public GameObject backpack;
  
  [SerializeField] 
  public GameObject vfx_step;
  [SerializeField]
  public GameObject vfx_exit_pulse;
  [SerializeField]
  public GameObject vfx_vertical_ray;
  [SerializeField]
  public AudioClip sfx_step;
  [SerializeField]
  public AudioClip ambient;
  
  [SerializeField]
  public float game_loop_time;
  
  [SerializeField] 
  private List<VoxelData> food_chain;
  public List<VoxelData> GetFoodChain() => food_chain;
  
  [SerializeField]
  public List<VoxelData> voxels_for_spawn;
  internal VoxelData GetRandomVoxelDataForSpawn()
  {
    return voxels_for_spawn[Random.Range(0,voxels_for_spawn.Count)];
  }
  
  public VoxelData GetFood(VoxelData voxel)
  {
    int idx = food_chain.IndexOf(voxel);
    if (idx < 0)
      return white;

    if (++idx > food_chain.Count - 1)
      idx = 0;

    return food_chain[idx];
  }

  Dictionary<Cube, Cube> pool_collided_cubes = new Dictionary<Cube, Cube>();

  bool IsPairExist(Cube cube_0, Cube cube_1)
  {
    Cube cube;
    return pool_collided_cubes.TryGetValue(cube_0, out cube) && cube == cube_1;
  }

  bool TryRegisteredPair(Cube cube_0, Cube cube_1)
  {
    if (cube_0.mover.is_dead || cube_1.mover.is_dead)
      return false;
    
    if (cube_0.is_exit() || cube_1.is_exit())
      return false;

    if (IsPairExist(cube_0, cube_1) || IsPairExist(cube_1, cube_0))
      return false;

    pool_collided_cubes[cube_0] = cube_1;
    return true;
  }
  
  public void ClearCollidedPair(Cube cube_0, Cube cube_1)
  {
    if (IsPairExist(cube_0, cube_1))
      pool_collided_cubes.Remove(cube_0);
    else if (IsPairExist(cube_1, cube_0))
      pool_collided_cubes.Remove(cube_1);
  }

  public void ResolveCollidedPair(Cube cube_0, Cube cube_1)
  {
    if (!TryRegisteredPair(cube_0, cube_1))
    {
//      Debug.Log("ResolveColliding interrupted");
      return;
    }

//    Debug.Log("ResolveColliding start resolving");
    int points_0 = cube_0.content.GetVoxelsCount();
    int points_1 = cube_1.content.GetVoxelsCount();
    
    int damage_0 = cube_0.content.GetDamagePoints();
    int hp_0 = cube_0.content.GetHpPoints() + damage_0;
    int damage_1 = cube_1.content.GetDamagePoints();
    int hp_1 = cube_1.content.GetHpPoints() + damage_1;

    Cube winner;
    Cube looser;

    if(points_0 != points_1)
    {
      winner = points_0 > points_1 ? cube_0 : cube_1;
      looser = points_0 > points_1 ? cube_1 : cube_0;
    }
    else if(hp_0 != hp_1)
    {
      winner = hp_0 > hp_1 ? cube_0 : cube_1;
      looser = hp_0 > hp_1 ? cube_1 : cube_0;
    }
    else
    {
      winner = cube_0;
      looser = cube_1;
    }

    int hp_winner = winner.content.GetHpPoints();
    int damage_looser = looser.content.GetDamagePoints();
    winner.content.RemoveVoxels(damage_looser);
    winner.content.AddVoxels(looser.content.GetVoxels(new List<VoxelData>{white, black}));
    looser.content.RemoveVoxels(hp_winner);

    if (winner.is_player())
    {
      looser.content.RemoveVoxels(1);
      looser.mover.Explode();
    }

    

//    if (is_destroyer())
//    {
//      if (enemy_cube.is_player())
//      {
//        if (enemy_cube.content.GetColoredCount() >= content.GetColoredCount())
//        {
//          enemy_cube.GetComponent<Player>().content.RemoveVoxels(my_points);
//          mover.Explode();
//        }
//        else
//          Game.GameOver(false, Color.clear);
//      }
//      else if (enemy_cube.is_marker() && enemy_cube.content.color != Color.black)
//        enemy_cube.mover.Explode();
//    }
//    else if (is_reverser())
//    {
//      var all_markers = FindObjectsOfType<Marker>();
//      for (int i = 0; i < all_markers.Length; i++)
//      {
//        var marker = all_markers[i];
//        marker.speed = marker.speed * -1;
//      }
//
//      mover.Take();
//    }
//    else if (enemy_cube.is_player())
//    {
//      if (content.color != Color.white)
//        enemy_cube.GetComponent<Player>().content.AddVoxels(content.GetVoxels());
//      else if (content.color == Color.white)
//        Game.self.IncreaseTime(10);
//
//      mover.Take();
//    }
  }
}

public abstract class SingletonSO<T> : ScriptableObject where T : ScriptableObject
{
  static T self_ = null;

  public static T self
  {
    get
    {
      if (!self_)
        self_ = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
      return self_;
    }
  }
}